'use strict'
// --!-- function def =============================


// --!-- init plugins =============================
$(document).ready(function () {
  // preloader
  $(window).on('load', function () {
    $('#preloader').fadeOut(function () {
      $(this).remove();
    });
    $('body').removeClass('no-scroll');
  });
  setTimeout(function () {
    if ($("#preloader").length) {
      $("#preloader").fadeOut(function () {
        $(this).remove();
      });
      $('body').removeClass('no-scroll');
    }
  }, 2000);
  // imgLiquid init
  $(".img-fill").imgLiquid({
    fill: true,
    horizontalAlign: "center",
    verticalAlign: "center"
  });
  $(".img-no-fill").imgLiquid({
    fill: false,
    horizontalAlign: "center",
    verticalAlign: "center"
  });
  // dotdotdot
  $('.dot, .dot-comment').dotdotdot({watch: true});

  // header
  const ps = new PerfectScrollbar('#header-scroll');
  $("header .header-menu-btn").on('click', function () {
    $("header .header-menu-btn").toggleClass('active');
    $(".header-menu .menu-wrap, .header-menu .overlay").toggleClass('shown');
    $("body, html").toggleClass('no-scroll');
  });
  $(".header-menu .overlay").on('click', function () {
    $("header .header-menu-btn").removeClass('active');
    $(".header-menu .menu-wrap, .header-menu .overlay").removeClass('shown');
    $("body, html").removeClass('no-scroll');
  });

  // pages logic ==================================
  if ($(".page-index").length) {
    var swiper = new Swiper('.swiper-container', {
      on: {
        init: function () {
          console.log('swiper initialized');
        }
      },
      loop: true,
      pagination: {
        el: '.swiper-pagination',
        dynamicBullets: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
      },
    });

    $("input[name='num_1']").TouchSpin({
      initval: 45,
      max: 999
    });
    $("input[name='num_2']").TouchSpin({
      initval: 2,
      max: 9
    });

    //$('.stepper-block').parallax();
  }
  if ($(".page-principles").length) {
    var swiper = new Swiper('.swiper-container', {
      on: {
        init: function () {
          console.log('swiper initialized');
        }
      },
      loop: true,
      pagination: {
        el: '.swiper-pagination',
        dynamicBullets: true,
      },
    });

    $("input[name='num_1']").TouchSpin({
      initval: 45,
      max: 999
    });
    $("input[name='num_2']").TouchSpin({
      initval: 2,
      max: 9
    });
  }
  if ($(".page-style").length) {

    $("input[name='num_1']").TouchSpin({
      initval: 45,
      max: 999
    });
    $("input[name='num_2']").TouchSpin({
      initval: 2,
      max: 9
    });
  }
  if ($(".page-gallery").length) {
    $('[data-fancybox="images"]').fancybox({
      baseClass: '.gallery-fancybox',
      idleTime: 0,
      infobar: false,
      buttons: ['close'],
      //parentEl: '.gallery-page .gallery-block',
      animationEffect: "fade",
      animationDuration: 300,
      btnTpl: {
        close: '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="{{CLOSE}}">' + '<svg viewBox="0 0 44 44">' + '<polygon style="fill:#FFFFFF;" points="44,1.419 42.581,0 22,20.581 1.419,0 0,1.419 20.581,22 0,42.581 1.419,44 22,23.419 42.581,44 44,42.581 23.419,22 "/>' + '</svg>' + '</button>',
        arrowLeft: '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}">' + '<svg viewBox="0 0 21 44">' + '<polygon style="fill:#FFFFFF;" points="20,0 21,1 2,21.992 21,43 20,44 0,21.964 "/>' + '</svg>' + '</button>',
        arrowRight: '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}">' + '<svg viewBox="0 0 21 44">' + '<polygon style="fill:#FFFFFF;" points="1,0 0,1 19,21.992 0,43 1,44 21,21.964 "/>' + '</svg>' + '</button>'
      },
      lang: 'ru',
      i18n: {
        'en': {
          CLOSE: 'Close',
          NEXT: 'Next',
          PREV: 'Previous',
          ERROR: 'The requested content cannot be loaded. <br/> Please try again later.',
          PLAY_START: 'Start slideshow',
          PLAY_STOP: 'Pause slideshow',
          FULL_SCREEN: 'Full screen',
          THUMBS: 'Thumbnails',
          DOWNLOAD: 'Download',
          SHARE: 'Share',
          ZOOM: 'Zoom'
        },
        'ru': {
          CLOSE: 'Закрыть',
          NEXT: 'Следующая',
          PREV: 'Предыдущая',
          ERROR: 'Ошибка при загрузке! <br/> Попробуйте перезагрузить страницу.',
          PLAY_START: 'Начать просмотр',
          PLAY_STOP: 'Остановить просмотр',
          FULL_SCREEN: 'Во всет экран',
          THUMBS: 'Предпросмотр',
          DOWNLOAD: 'Загрузить',
          SHARE: 'Поделиться',
          ZOOM: 'Увеличить'
        }
      }
    });

    var grid = $('.grid').masonry({
      columnWidth: '.grid-sizer',
      itemSelector: '.item-grid'
    });
  }
  if ($(".page-contacts").length) {
    $("#inputPhone").mask("?+7 (999) 999-9999");
  }
  if ($(".page-calc").length) {

    $("input[name='num_1']").TouchSpin({
      initval: 45,
      max: 999
    });
    $("input[name='num_2']").TouchSpin({
      initval: 2,
      max: 9
    });


    $(".option-picker-wrap .checkbox input").on("change", function () {
      //$(this).parents("li").find(".option-img-wrap").toggleClass('shown');
      $(this).parents("li").find(".option-size").toggleClass('shown');
    });
    $(".option-touchspin input").TouchSpin({
      initval: 1,
    });

    $('[data-fancybox="images"]').fancybox({
      baseClass: '.gallery-fancybox',
      idleTime: 0,
      infobar: false,
      buttons: ['close'],
      //parentEl: '.gallery-page .gallery-block',
      animationEffect: "fade",
      animationDuration: 300,
      btnTpl: {
        close: '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="{{CLOSE}}">' + '<svg viewBox="0 0 44 44">' + '<polygon style="fill:#FFFFFF;" points="44,1.419 42.581,0 22,20.581 1.419,0 0,1.419 20.581,22 0,42.581 1.419,44 22,23.419 42.581,44 44,42.581 23.419,22 "/>' + '</svg>' + '</button>',
        arrowLeft: '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}">' + '<svg viewBox="0 0 21 44">' + '<polygon style="fill:#FFFFFF;" points="20,0 21,1 2,21.992 21,43 20,44 0,21.964 "/>' + '</svg>' + '</button>',
        arrowRight: '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}">' + '<svg viewBox="0 0 21 44">' + '<polygon style="fill:#FFFFFF;" points="1,0 0,1 19,21.992 0,43 1,44 21,21.964 "/>' + '</svg>' + '</button>'
      },
      lang: 'ru',
      i18n: {
        'en': {
          CLOSE: 'Close',
          NEXT: 'Next',
          PREV: 'Previous',
          ERROR: 'The requested content cannot be loaded. <br/> Please try again later.',
          PLAY_START: 'Start slideshow',
          PLAY_STOP: 'Pause slideshow',
          FULL_SCREEN: 'Full screen',
          THUMBS: 'Thumbnails',
          DOWNLOAD: 'Download',
          SHARE: 'Share',
          ZOOM: 'Zoom'
        },
        'ru': {
          CLOSE: 'Закрыть',
          NEXT: 'Следующая',
          PREV: 'Предыдущая',
          ERROR: 'Ошибка при загрузке! <br/> Попробуйте перезагрузить страницу.',
          PLAY_START: 'Начать просмотр',
          PLAY_STOP: 'Остановить просмотр',
          FULL_SCREEN: 'Во всет экран',
          THUMBS: 'Предпросмотр',
          DOWNLOAD: 'Загрузить',
          SHARE: 'Поделиться',
          ZOOM: 'Увеличить'
        }
      }
    });
    // var grid = $('.grid').masonry({
    //   columnWidth: '.grid-sizer',
    //   itemSelector: '.item-grid'
    // });
  }

});